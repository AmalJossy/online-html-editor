import { Moon, Sun } from "react-feather";
import styles from "./styles.module.css";

type HeaderProps = {
  darkMode: boolean;
  setDarkMode: (arg0: boolean) => void;
};
function Header(props: HeaderProps) {
  const { darkMode, setDarkMode } = props;
  return (
    <div className={styles.Header__Container}>
      <h3>Html Editor</h3>
      <button
        className={styles.Header__Button}
        onClick={() => setDarkMode(!darkMode)}
        title={`Switch to ${darkMode ? "Light Mode" : "Dark Mode"}`}
      >
        {darkMode ? (
          <Sun height={18} width={18} />
        ) : (
          <Moon height={18} width={18} />
        )}
      </button>
    </div>
  );
}

export default Header;
