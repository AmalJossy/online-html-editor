import { useRef, useState } from "react";
import { Save, Play, RefreshCw } from "react-feather";
import AceEditor from "react-ace";
import { saveAs } from "file-saver";

import Editor from "../Editor";
import Preview from "../Preview";
import styles from "./styles.module.css";

const saveFile = (text: string | undefined) => {
  if (text) {
    var file = new File([text], "download.txt", {
      type: "text/plain;charset=utf-8",
    });
    saveAs(file);
  }
};
const initialContent = ``;

function EditorWithPreview() {
  const [content, setContent] = useState(initialContent);
  const [previewContent, setPreviewContent] = useState(initialContent);
  const [livePreviewFlag, setLivePreviewFlag] = useState(false);
  const aceRef = useRef<AceEditor>(null);

  const handleChange = (value: string) => {
    if (livePreviewFlag) {
      setPreviewContent(value);
    }
    setContent(value);
  };
  return (
    <div className={styles.EditorWithPreview__Container}>
      <header className={styles.EditorWithPreview__Header}>
        <button
          title="Save"
          onClick={() => {
            saveFile(aceRef.current?.editor.getValue());
          }}
        >
          <Save  height={18} width={18}/>
        </button>
        <button
          title="Run"
          disabled={livePreviewFlag}
          onClick={() => {
            setPreviewContent(content);
          }}
        >
          <Play  height={18} width={18}/>
        </button>
        <button
          title="Live Preview"
          aria-pressed={livePreviewFlag}
          onClick={() => setLivePreviewFlag(!livePreviewFlag)}
        >
          <RefreshCw  height={18} width={18}/>
        </button>
      </header>
      <main className={styles.EditorWithPreview__Body}>
        <div className={styles.EditorWithPreview__Editor}>
          <Editor content={content} onChange={handleChange} ref={aceRef} />
        </div>
        <div className={styles.EditorWithPreview__Preview}>
          <Preview content={previewContent} />
        </div>
      </main>
    </div>
  );
}

export default EditorWithPreview;
