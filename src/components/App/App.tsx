import { useState } from "react";
import CoreLayout from "../CoreLayout";
import EditorWithPreview from "../EditorWithPreview";
import Header from "../Header";
import styles from "./styles.module.css";
function App() {
  const [darkMode, setDarkMode] = useState(true);
  return (
    <div className={`${styles.App__Container} ${darkMode ? styles["App--dark"] : styles["App--light"]}`}>
      <CoreLayout
        header={<Header darkMode={darkMode} setDarkMode={setDarkMode} />}
        main={<EditorWithPreview />}
      />
    </div>
  );
}

export default App;
