import styles from "./styles.module.css";

type PreviewProps = {
  content: string;
};
function Preview(props: PreviewProps) {
  const { content } = props;
  const writeContent = (frame: HTMLIFrameElement) => {
    if (!frame) return;
    let doc = frame.contentDocument;
    if (frame.contentWindow) doc = frame.contentWindow.document;
    if (!doc) return;
    doc.open();
    doc.write(content);
    doc.close();
  };
  return (
    <div className={styles.Preview__Container}>
      <iframe
        title="Page Preview"
        ref={writeContent}
        className={styles.Preview__IFrame}
      />
    </div>
  );
}

export default Preview;
