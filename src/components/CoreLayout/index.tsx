import styles from "./styles.module.css"

type CoreLayoutProps = {
  header: React.ReactNode;
  main: React.ReactNode;
};

function CoreLayout(props: CoreLayoutProps) {
  const { header, main } = props;
  return (
    <div className={styles.CoreLayout__Container}>
      <header className={styles.CoreLayout__Header}>{header}</header>
      <main className={styles.CoreLayout__Main}>{main}</main>
    </div>
  );
}

export default CoreLayout;
