import AceEditor from "react-ace";
import "ace-builds/webpack-resolver"; // causes bloat TODO: find a fix
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";

import styles from "./styles.module.css";
import React from "react";

type EditorProps = {
  content: string;
  onChange: (arg0: string) => void;
};

function Editor(props: EditorProps, ref: React.LegacyRef<AceEditor>) {
  const { content, onChange } = props;
  return (
    <div className={styles.Editor__Container}>
      <AceEditor
        ref={ref}
        placeholder="Write your code here"
        mode="html"
        theme="monokai"
        name="ace-editor"
        className={styles.Editor__Ace}
        // onLoad={this.onLoad}
        onChange={(value) => onChange(value)}
        fontSize={14}
        showPrintMargin={true}
        showGutter={true}
        highlightActiveLine={true}
        value={content}
        style={{ width: "100%" }}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: false,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
        }}
      />
    </div>
  );
}

export default React.forwardRef(Editor);
